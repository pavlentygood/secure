package com.example.secure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled=true)
//@FieldDefaults(level = PRIVATE, makeFinal = true)
class SecurityConfig extends WebSecurityConfigurerAdapter {
  private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
    new AntPathRequestMatcher("/auth/**")
  );
  private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

  private UserDetailsService userDetailsService;

  private TokenAuthenticationProvider provider;

  public SecurityConfig(UserDetailsService userDetailsService, final TokenAuthenticationProvider provider) {
    super();
    this.userDetailsService = userDetailsService;
    this.provider = provider;
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService);
//    auth.authenticationProvider(provider);
  }

//  @Override
//  public void configure(final WebSecurity web) {
//    web.ignoring().requestMatchers(PUBLIC_URLS);
//  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
            .csrf().disable()
            .httpBasic()
            .and()
            .sessionManagement()
            .sessionCreationPolicy(STATELESS)
            .and()
//            .addFilterAfter(restAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
            .antMatchers("/login")
            .authenticated();

//            .csrf().ignoringAntMatchers("/**")
//            .and()
//      .exceptionHandling()
      // this entry point handles when you request a protected page and you are not yet
      // authenticated
//      .defaultAuthenticationEntryPointFor(forbiddenEntryPoint(), PROTECTED_URLS)
//      .and()
//      .authenticationProvider(provider)
//      .addFilterBefore(restAuthenticationFilter(), AnonymousAuthenticationFilter.class)
//      .authorizeRequests()
//      .requestMatchers(PROTECTED_URLS)
//      .authenticated()

//      .and()
//      .csrf().disable()
//      .formLogin().disable()
//      .httpBasic().disable()
//      .logout().disable();
  }

  @Bean
  TokenAuthenticationFilter restAuthenticationFilter() throws Exception {
    final var filter = new TokenAuthenticationFilter(PROTECTED_URLS);
    filter.setAuthenticationManager(authenticationManager());
    filter.setAuthenticationSuccessHandler(successHandler());
    return filter;
  }

  @Bean
  SimpleUrlAuthenticationSuccessHandler successHandler() {
    final SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
    successHandler.setRedirectStrategy(new NoRedirectStrategy());
    return successHandler;
  }

  /**
   * Disable Spring boot automatic filter registration.
   */
//  @Bean
//  FilterRegistrationBean disableAutoRegistration(final TokenAuthenticationFilter filter) {
//    final FilterRegistrationBean registration = new FilterRegistrationBean(filter);
//    registration.setEnabled(false);
//    return registration;
//  }

//  @Bean
//  AuthenticationEntryPoint forbiddenEntryPoint() {
//    return new HttpStatusEntryPoint(FORBIDDEN);
//  }

}
