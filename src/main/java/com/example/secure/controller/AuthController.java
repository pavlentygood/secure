package com.example.secure.controller;

import com.example.secure.service.TokenService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AuthController {

    private final TokenService tokenService;

    public AuthController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @PostMapping("login")
    public ResponseEntity<String> login() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        User user = (principal instanceof User) ? (User) principal : null;

        var token = tokenService.permanent(Map.of("username", user.getUsername()));

        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, token)
                .body(user.getUsername());
    }

    @GetMapping("users")
    public String getUsers() {
        return "users list !";
    }
}
